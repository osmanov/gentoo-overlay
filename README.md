# Warning

**The project had been moved to [https://github.com/rosmanov/gentoo-overlay](https://github.com/rosmanov/gentoo-overlay)**

# About

This is a Gentoo overlay for my projects and for packages missing in official portage.

# Adding the overlay

```
layman -f -o https://bitbucket.org/osmanov/gentoo-overlay/raw/master/repository.xml -a osmanov-overlay
```
